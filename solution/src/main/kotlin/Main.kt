package com.pajato.katas.marsrover.oo

import java.lang.Exception

typealias Point = Pair<Int, Int>

class MarsRover(
    internal var position: Point = Point(0, 0),
    internal var direction: Char = 'N',
    private var obstacles: List<Point> = listOf()
) {
    fun execute(input: String): String {
        fun update(action: Char) {
            fun nextDirection() = when {
                (direction == 'N' && action == 'R') || (direction == 'S' && action == 'L') -> 'E'
                (direction == 'E' && action == 'R') || (direction == 'W' && action == 'L') -> 'S'
                (direction == 'S' && action == 'R') || (direction == 'N' && action == 'L') -> 'W'
                else -> 'N'
            }
            fun nextPosition(): Point {
                fun nextX(x: Int) = when (direction) {
                    'W' -> if (x == 0) 9 else x - 1
                    'E' -> if (x == 9) 0 else x + 1
                    else -> x
                }
                fun nextY(y: Int) = when (direction) {
                    'N' -> if (y == 9) 0 else y + 1
                    'S' -> if (y == 0) 9 else y - 1
                    else -> y
                }

                val result = Point(nextX(position.first), nextY(position.second))
                if (obstacles.contains(result)) throw ObstacleException()
                return result
            }

            if (action == 'R' || action == 'L') direction = nextDirection()
            if (action == 'M') position = nextPosition()
        }
        fun processInputs(): String {
            input.forEach {action -> update(action) }
            return "${position.first}:${position.second}:$direction"
        }

        return try {
            processInputs()
        } catch (exc: ObstacleException) {
            "O:${position.first}:${position.second}:$direction"
        }
    }
}

class ObstacleException : Exception()
